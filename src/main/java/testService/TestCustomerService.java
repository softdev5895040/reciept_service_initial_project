/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package testService;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author USER
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cusService = new CustomerService();
        
        for (Customer customer:cusService.getCustomers()) {
            System.out.println(customer.toString());
            
        }
        
        Customer newCus = new Customer("Pom", "0813064437");
        cusService.addNew(newCus);
        for (Customer customer:cusService.getCustomers()) {
            System.out.println(customer.toString());
            
        }
        System.out.println("Get by tel : "+cusService.getByTel("0815593836"));
        cusService.delete(newCus);
        for (Customer arg : cusService.getCustomers()){
            System.out.println(arg);
            
            
        }
    }
    
}
